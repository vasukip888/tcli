/*
 * Copyright (c) 2015 Appfire Technologies, Inc.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Bob Swift Atlassian Add-ons EULA"
 * (https://bobswift.atlassian.net/wiki/x/WoDXBQ) as well as under the provisions of
 * the "Standard EULA" from the "Atlassian Marketplace Terms of Use" as a "Marketplace Product”
 * (http://www.atlassian.com/licensing/marketplace/termsofuse).
 *
 * See the LICENSE file for more details.
 */

package testyml;

public class YamlTesting {
    public static void main(String[] args) {
        // Yaml yaml = new Yaml();
        // try (InputStream in = YamlTesting.class.getResourceAsStream("user.yaml")) {
        // Object obj = yaml.load(in);
        // System.out.println("Loaded object type:" + obj.getClass());
        // System.out.println(obj);
        // } catch (IOException e) {
        // e.printStackTrace();
        // }

        String jsonDate = "2019-11-13T11:42:32-0500";
        System.out.println(jsonDate.length());
        if (jsonDate.charAt(19) == '-') { // 2019-11-13T11:42:32-0500 for archive date in data center
            jsonDate = jsonDate.substring(0, 19) + ".000" + jsonDate.substring(19, 24);
        }
        System.out.println(jsonDate + "\n");
        System.out.println(jsonDate.length());
    }
}
